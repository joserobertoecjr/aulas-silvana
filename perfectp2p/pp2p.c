 /* 
 * Algorithm PerfectP2P 
 */

#include "pp2p.h"
#include "fairp2p.h"

//ports to receive and send messages
int sendPort;
int recvPort;

typedef struct Msg{
	int id;
	char content[100];
} Msg; //struct para mensagem

typedef struct BufR{
	Msg msg;
	char src[20];
	int times; //quantas vezes foi recebida
} BufR; //struct para o Buffer que vai guardar as mensagens rececbidas

typedef struct BufS{
	Msg msg;
	char dest[20];
	int times; //quantas vezes foi enviado
} BufS; //struct para o Buffer que vai guardar as mensagens enviadas

BufR received[100];
int nrc = 0; //numero de msgs recebidas

BufS sended[10];
int nm = 0; //numero de mensagens enviadas

// Request PerfectP2P event: send
void pp2pSend (char *dest, char *msg)
{

	sended[nm%10].msg.id = nm;
	strcpy(sended[nm%10].msg.content, msg); //guardo mensagem nas mensagens enviadas
	strcat(sended[nm%10].msg.content, "\n");
	strcpy(sended[nm%10].dest, dest);
	strcat(sended[nm%10].dest, "\n");
	sended[nm%10].times = 1;

	fp2pSend(dest, msg);

	printf("Mensagem %d enviada 1 vez\n", sended[nm%10].msg.id);

	nm++;
	
}

void * sender()
{
	int i = 0;
	
	printf("Pronto para enviar.\n");
	
	for(;;){
		for(i=0;i<nm || i<10;i++)
		{
			if(sended[i].times < 10){
				printf("Mensagem %d reeviada %d vezes\n", sended[i].msg.id, sended[i].times);
				fp2pSend("127.0.0.1", sended[i].msg.content);
				sended[i].times++;
			}
			
		}
		sleep(0.3);
	}
	pthread_exit(NULL);
}

// receive a message
int pp2pReceive (char *src, char *msg)
{
	int i;

	for(i=0;i<nrc;i++){
		if(received[i].msg.id == atoi(msg)){
			if(received[i].times == 0){
				received[i].times = 1;
			} else{
				received[i].times++;
			}
		}
	}
	
	/* return  the  number  of bytes received, or -1 if an error occurred */
	return strlen(msg) * sizeof(char);
}

void fp2pDelivery (char *src, char *msg)
{
	if (pp2pReceive(src, msg) > 0)
		pp2pDelivery(src, msg);
}

// PerfectP2P init event
pthread_t pp2pInit(int pp2psendPort, int pp2precvPort)
{
	
    sendPort = pp2psendPort; //port to send messages to dst nodes
    recvPort = pp2precvPort; //port to receive messages from src nodes
    
	if(recvPort == 4000){
		pthread_t tid;
		if(pthread_create(&tid, NULL, sender, NULL))
		   error("error pthread_create()");
	}
 
    return fp2pInit(sendPort, recvPort);
}
