// A multithread echo server 

#include "mysocket.h"  
#include <pthread.h>

#define NTHREADS 100

int tid = 0;

/* Structure of arguments to pass to client thread */
struct TArgs {
  TSocket cliSock;   /* socket descriptor for client */
};

/* Handle client request */
void * HandleRequest(void *args) {
  char str[100];
  TSocket cliSock;

  /* Extract socket file descriptor from argument */
  cliSock = ((struct TArgs *) args) -> cliSock;
  free(args);  /* deallocate memory for argument */

  /* variaveis calculadora */

  char res[100]; //string de resposta final
  char *str1; //string para armazenar primeiro numero
  char *str2; //string para armazenar segundo numero

  char *ptr; //ponteiro oara armazenar restos de string
  long num1; //long para armazenar primeiro numero
  long num2; //long para armazenar segundo numero
  long result; //reultado em long que sera passado para char*

  char opr[4][2];
  strcpy(opr[0], "+\0"); //array de operador de soma
  strcpy(opr[1], "-\0"); //array de operador de subtracao
  strcpy(opr[2], "*\0"); //array de operador de multiplicacao
  strcpy(opr[3], "/\0"); //array de operador de divisao

  int i;

  /* fim variaveis calculadora */

  for(;;) {
    
    /* Receive the request */
    if (ReadLine(cliSock, str, 99) < 0) 
      { ExitWithError("ReadLine() failed"); 
    } else printf("%s",str);
    if (strncmp(str, "quit", 4) == 0){
      tid--;
      break;
    }
 
    /* calculadora */

    for(i = 0; i < 4; i++){ //passando por todas operacoes
      
      printf("str len  %ld\n", strlen(str));

      if(strlen(str) == 8){
        if(str[0] == 'c' && str[1] == 'l' && str[2] == 'i' && str[3] == 'e' && str[4] == 'n' && str[5] == 't' && str[6] == 's'){
          sprintf(res, "%d\n", tid);
        break;
        }
      }

      if(strstr(str, opr[i])){ //buscando a operacao

        //pegando os dois numeros da operacao
        str1 = strtok(str, opr[i]); //pego primeiro numero da operacao
        str2 = strtok(NULL, opr[i]); //pego segundo numero da operacao
        num1 = strtol(str1, &ptr, 10); //transformo a str1 em long
        num2 = strtol(str2, &ptr, 10); //transformo a str2 em long
        
        if(i == 0){
          result = num1+num2; //somo os numeros pegos  
        } else if(i == 1){
          result = num1-num2; //subtraio os numeros pegos
        } else if(i == 2){
          result = num1*num2; //multiplico os numeros pegos
        } else if(i == 3){
          result = num1/num2; //divido os numeros pegos
        }
        sprintf(res, "%ld\n", result); //transformo resultado em string
        break; //parando o for
      }
    }

    /* fim calculadora */

    /* Send the response */
    if (WriteN(cliSock, res, strlen(res)) <= 0)  
      { ExitWithError("WriteN() failed"); } 
  }

  close(cliSock);

  pthread_exit(NULL);

}

int main(int argc, char *argv[]) {
  TSocket srvSock, cliSock;        /* server and client sockets */
  struct TArgs *args;              /* argument structure for thread */
  pthread_t threads[NTHREADS];

  if (argc == 1) { ExitWithError("Usage: server <local port>"); }

  /* Create a passive-mode listener endpoint */  
  srvSock = CreateServer(atoi(argv[1]));

  /* Run forever */
  for (;;) { 
    if (tid == NTHREADS) 
      { ExitWithError("number of threads is over"); }

    /* Spawn off separate thread for each client */
    cliSock = AcceptConnection(srvSock);

    /* Create separate memory for client argument */
    if ((args = (struct TArgs *) malloc(sizeof(struct TArgs))) == NULL)
      { ExitWithError("malloc() failed"); }
    args->cliSock = cliSock;

    /* Create a new thread to handle the client requests */
    if (pthread_create(&threads[tid++], NULL, HandleRequest, (void *) args)) {
      { ExitWithError("pthread_create() failed"); }
    }
    /* NOT REACHED */
  }
}
