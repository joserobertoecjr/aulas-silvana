// A simple ack server 
#include "mysocket.h"  

/* Structure of arguments to pass to client thread */
struct TArgs {
  TSocket cliSock;   /* socket descriptor for client */
};

/* Handle client request */
int HandleRequest(void *args) {
  char str[100];
  TSocket cliSock;

  /* Extract socket file descriptor from argument */
  cliSock = ((struct TArgs *) args) -> cliSock;
  free(args);  /* deallocate memory for argument */

  /* Receive the request */
  if (ReadLine(cliSock, str, 99) < 0) 
    { ExitWithError("ReadLine() failed"); 
  } else printf("%s",str);  
  
  /* calculadora */

  char res[100]; //string de resposta final
  char *str1; //string para armazenar primeiro numero
  char *str2; //string para armazenar segundo numero

  char *ptr; //ponteiro oara armazenar restos de string
  long num1; //long para armazenar primeiro numero
  long num2; //long para armazenar segundo numero
  long result; //reultado em long que sera passado para char*

  char opr[4][2];
  strcpy(opr[0], "+\0"); //array de operador de soma
  strcpy(opr[1], "-\0"); //array de operador de subtracao
  strcpy(opr[2], "*\0"); //array de operador de multiplicacao
  strcpy(opr[3], "/\0"); //array de operador de divisao

  int i;
  for(i = 0; i < 4; i++){ //passando por todas operacoes
    
    if(strstr(str, opr[i])){ //buscando a operacao

      //pegando os dois numeros da operacao
      str1 = strtok(str, opr[i]); //pego primeiro numero da operacao
      str2 = strtok(NULL, opr[i]); //pego segundo numero da operacao
      num1 = strtol(str1, &ptr, 10); //transformo a str1 em long
      num2 = strtol(str2, &ptr, 10); //transformo a str2 em long
      
      if(i == 0){
        result = num1+num2; //somo os numeros pegos  
      } else if(i == 1){
        result = num1-num2; //subtraio os numeros pegos
      } else if(i == 2){
        result = num1*num2; //multiplico os numeros pegos
      } else if(i == 3){
        result = num1/num2; //divido os numeros pegos
      }
      sprintf(res, "%ld", result); //transformo resultado em string
      break; //parando o for
    }
  }

  /* fim calculadora */
  
  /* Send the response */
  if (WriteN(cliSock, res, strlen(res)) <= 0)  
    { ExitWithError("WriteN() failed"); }  

  close(cliSock);
  return 1;
}

int main(int argc, char *argv[]) {
  TSocket srvSock, cliSock;        /* server and client sockets */
  struct TArgs *args;              /* argument structure for thread */

  if (argc == 1) {
    ExitWithError("Usage: server <local port>");    
  }

  /* Create a passive-mode listener endpoint */  
  srvSock = CreateServer(atoi(argv[1]));

  for (;;) { /* run forever */
    cliSock = AcceptConnection(srvSock);

    /* Create a memory space for client argument */
    if ((args = (struct TArgs *) malloc(sizeof(struct TArgs))) == NULL)
      { ExitWithError("malloc() failed"); }
    args->cliSock = cliSock;

    /* Handle the client request */
    if (HandleRequest((void *) args) != 1)
      { ExitWithError("HandleRequest() failed"); }
  }
}
