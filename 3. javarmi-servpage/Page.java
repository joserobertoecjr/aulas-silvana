// Programa Java com RMI
// Page.java - Interface compartilhada entre cliente e servidor
//-------------------------------------------------------------

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Page extends Remote {

  // Requisicao de uma pagina
  public int request () throws RemoteException;
  public int calcula(String entrada, int num1, int num2) throws RemoteException;
  // Liberaracao de uma pagina
  public void release (int id) throws RemoteException;
}
