// Programa Java com RMI
// PageClient.java - Classe usuaria de paginas
//-------------------------------------------------------------

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;
import java.util.InputMismatchException;

public class PageClient {
  
  static Scanner scan = new Scanner(System.in);
  static String operacao; 
  static int resposta;
  static int num1;
  static int num2;

  private PageClient() {}
  // Metodo principal
  public static void main (String[] args) {
    int pagina;

    // Especifica o nome do servidor e do objeto para obter um stub para acessar o objeto servidor
    String host = (args.length < 1) ? null : args[0];
    try {
       Registry registry = LocateRegistry.getRegistry(host);
       Page stub = (Page) registry.lookup("Page");
       for(;;) {

          scan = new Scanner(System.in);

          //le a entrada
          System.out.println("Qual operacao deseja?");
          operacao = scan.nextLine();

          //le o primeiro numero
          System.out.println("Qual o primeiro numero da operacao?");
          num1 = scan.nextInt();
          //System.out.println(scan.nextLine());

          //le o segundo numero
          System.out.println("Qual o segundo numero da operacao?");
          num2 = scan.nextInt();

          resposta = stub.calcula(operacao, num1, num2);

          System.out.println("A resposta da operacao eh " + resposta + " \n");
       }
    } catch (Exception e) {
       System.err.println("Erro cliente: " + e.toString());
       e.printStackTrace();
    }
  }
}
